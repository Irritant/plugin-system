package org.bitbucket.irritant.test.exampleplugin;

import org.bitbucket.irritant.pluginsystem.Plugin;
import org.bitbucket.irritant.pluginsystem.PluginInfo;

@PluginInfo(
		priority = 6,
		creator = "Sparks",
		created = "29/07/2015")
public class PluginExample implements Plugin {

	@Override
	public void onLoad() {
		System.out.println("PluginExample has been loaded!");
	}

	@Override
	public String getPluginName() {
		return "PluginExample";
	}
}
