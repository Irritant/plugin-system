package org.bitbucket.irritant.pluginsystem;

public interface Plugin {

	void onLoad();
	String getPluginName();
}
