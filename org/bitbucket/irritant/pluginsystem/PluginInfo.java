package org.bitbucket.irritant.pluginsystem;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PluginInfo {
	
	byte priority() default 5;
	String creator() default "null";
	String created() default "DD/MM/YYYY";
	String[] contactInfo() default "Skype: null";
}
