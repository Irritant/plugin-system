package org.bitbucket.irritant.pluginsystem.exception;

public class PluginLoadException extends Exception {

	private static final long serialVersionUID = 1L;

	public PluginLoadException(Exception ex) {
		super(ex);
	}
}
