package org.bitbucket.irritant.pluginsystem.exception;

public class PluginNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public PluginNotFoundException(String pluginName) {
		super("Could not find plugin: " + pluginName);
	}
}
