package org.bitbucket.irritant.pluginsystem;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.bitbucket.irritant.pluginsystem.exception.PluginLoadException;
import org.bitbucket.irritant.pluginsystem.exception.PluginNotFoundException;

/**
 * This API is used for loading jars and class files
 * from a directory.
 * @author Sparks
 * @since 27/07/2015
 * @version 0.1
 */
public class PluginManager {

	private static final String NAME = "[PluginManager]";
	private File pluginDir;
	private Map<String, PluginStorage> pluginMap = new TreeMap<String, PluginStorage>();
	private final List<File> pluginFileList = new ArrayList<File>();
	
	public PluginManager(String pluginDir) {
		this.pluginDir = new File(pluginDir);
		File[] pluginDirFiles = this.pluginDir.listFiles(new PluginNameFilter());
		
		for(File file : pluginDirFiles) {
			if(file.isFile()) {
				logger(file.getName() + " has been found.");
				pluginFileList.add(file);
			}
		}
	}
	
	public void loadAllPlugins() throws PluginLoadException {
		List<PluginStorage> storageList = new ArrayList<PluginStorage>();
		for(File file : pluginFileList) {
			if(file.getName().endsWith(".class"))
				storageList.add(new PluginStorage(this.loadClassFile(file)));
			else if(file.getName().endsWith(".jar"))
				storageList.add(new PluginStorage(this.loadJarFile(file)));
		}
		
		Collections.sort(storageList, new Comparator<PluginStorage>() {
			@Override
			public int compare(PluginStorage arg0, PluginStorage arg1) {
				return arg1.getPluginPriority() - arg0.getPluginPriority();
			}
		});
		
		for(PluginStorage storage : storageList) {
			pluginMap.put(storage.getPlugin().getPluginName(), storage);
			storage.getPlugin().onLoad();
			logger(storage.getPluginName() + " has been loaded!");
		}
		storageList.clear();
	}
	
	public boolean unloadAllPlugins() {
		pluginMap.clear();
		if(pluginMap.isEmpty())
			return true;
		return false;
	}
	
	public Plugin loadClassPlugin(String pluginName) throws PluginNotFoundException, PluginLoadException {
		for(File file : pluginFileList) {
			String fileName = file.getName().toLowerCase();
			if(fileName.contains(pluginName) && fileName.endsWith(".class")) {
				return this.loadClassFile(file);
			}
		}
		throw new PluginNotFoundException(pluginName);
	}
	
	public Plugin loadJarPlugin(String pluginName) throws PluginNotFoundException, PluginLoadException {
		for(File file : pluginFileList) {
			String fileName = file.getName().toLowerCase();
			if(fileName.contains(pluginName) && fileName.endsWith(".jar")) {
				return this.loadJarFile(file);
			}
		}
		throw new PluginNotFoundException(pluginName);
	}
	
	public boolean unload(String pluginName) {
		if(pluginMap.containsKey(pluginName)) {
			pluginMap.remove(pluginName);
			return true;
		}
		return false;
	}
	
	
	public PluginStorage getPluginByName(String pluginName) {
		if(pluginMap.containsKey(pluginName)) {
			return pluginMap.get(pluginName);
		}
		return null;
	}
	
	public boolean isPluginLoaded(String pluginName) {
		if(pluginMap.containsKey(pluginName)) {
			return true;
		}
		return false;
	}
	
	public void reloadFileList() {
		pluginFileList.clear();
		
		File[] pluginDirFiles = this.pluginDir.listFiles(new PluginNameFilter());
		
		for(File file : pluginDirFiles) {
			if(file.isFile()) {
				logger(file.getName() + " has been found.");
				pluginFileList.add(file);
			}
		}
	}
	
	private Plugin loadClassFile(File classFile) throws PluginLoadException {
		ByteClassLoader classLoader = new ByteClassLoader(classFile);
		Class<?> clazz = classLoader.getClassFromByteArray();
		if(Plugin.class.isAssignableFrom(clazz)) {
			Plugin plugin;
			try {
				plugin = (Plugin)clazz.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new PluginLoadException(e);
			}
			return plugin;
		}
		return null;
	}
	
	private Plugin loadJarFile(File jarFile) throws PluginLoadException {
		try {
			JarFile jar = new JarFile(jarFile);
			for (Enumeration<?> e = jar.entries(); e.hasMoreElements(); ) {
				JarEntry jarEntry = (JarEntry) e.nextElement();
				if(!jarEntry.isDirectory() &&  jarEntry.getName().endsWith(".class")) {
					String clazz = jarEntry.getName().replaceAll("/", ".").replace(".class", "");
					URL url = new URL("jar:file:" + jarFile.getAbsolutePath() + "!/");
					URLClassLoader urlLoader = new URLClassLoader(new URL[] {url}, this.getClass().getClassLoader());
					Class<?> pluginCheck = urlLoader.loadClass(clazz);
					urlLoader.close();
					jar.close();
					if(Plugin.class.isAssignableFrom(pluginCheck)) {
						Plugin plugin = (Plugin)pluginCheck.newInstance();
						return plugin;
					}
				}
			}
		} catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e1) {
			throw new PluginLoadException(e1);
		}
		return null;
	}
	
	/**
	 * Loads a class from a byte array.
	 * @author Sparks, Bibl and CyberBrick
	 * Bibl for helping me around a deprecated method &
	 * CyberBrick for helping me in general.
	 */
	private final class ByteClassLoader extends ClassLoader {
		
		private File clazzFile;
		private byte[] byteArray;
		
		public ByteClassLoader(File clazzFile) {
			this.clazzFile = clazzFile;
			this.byteArray = getByteArrayFromFile();
		}
		
		public Class<?> getClassFromByteArray() {
			Class<?> clazz = this.defineClass(null, byteArray, 0, byteArray.length);
			return clazz;
		}
		
		private byte[] getByteArrayFromFile() {
			try {
				InputStream is = new FileInputStream(clazzFile);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				int r = 0;
				byte[] buffer = new byte[1024];
				while ((r = is.read(buffer)) >= 0) {
					baos.write(buffer, 0, r);
				}
				is.close();
				return baos.toByteArray();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}
	}
	
	public final class PluginStorage {
		
		private Plugin pluginInstance;
		private PluginInfo pluginInfo;
		
		public PluginStorage(Plugin pluginInstance) {
			this.pluginInstance = pluginInstance;
			pluginInfo = pluginInstance.getClass().getDeclaredAnnotation(PluginInfo.class);
		}
		
		public Plugin getPlugin() {
			return pluginInstance;
		}
		
		public String getPluginName() {
			return pluginInstance.getPluginName();
		}
		
		public byte getPluginPriority() {
			return pluginInfo.priority();
		}
		
		public String getPluginCreator() {
			return pluginInfo.creator();
		}
		
		public String getPluginDate() {
			return pluginInfo.created();
		}
		
		public String[] getPluginContactInfo() {
			return pluginInfo.contactInfo();
		}
	}
	
	private final class PluginNameFilter implements FilenameFilter {
		@Override
		public boolean accept(File dir, String name) {
			if(name.toLowerCase().endsWith(".jar") || name.toLowerCase().endsWith(".class"))
				return true;
			return false;
		}
	}
	
	/**
	 * @return The loaded plugin map.
	 */
	public Map<String, PluginStorage> getLoadedPluginMap() {
		return this.pluginMap;
	}
	
	/**
	 * @return The plugin file list.
	 */
	public List<File> getPluginFileList() {
		return this.pluginFileList;
	}
	
	/**
	 * @return The plugin directory.
	 */
	public File getPluginDir() {
		return pluginDir;
	}
	
	private void logger(Object... object) {
		for(Object obj : object) {
			System.out.println(NAME + ": " + obj);
		}
	}
}
